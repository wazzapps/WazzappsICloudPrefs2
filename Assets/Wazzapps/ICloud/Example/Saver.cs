﻿using UnityEngine;
using Wazzapps.ICloud;

public class Saver : MonoBehaviour
{
    public UnityEngine.UI.Text text;

    void Awake()
    {
        text.text = "";
    }

    public void SyncToCloud()
    {
        PlayerPrefsICloud.SaveToCloud();
    }

    public void ReadFromCloud()
    {
        PlayerPrefsICloud.ReadFromCloud(OnCloudLoadedHandler);
    }

    public void Check()
    {
        bool res = PlayerPrefsICloud.IsCloudEnabled();
        Debug.Log("Enabled " + res);
        text.text = "Enabled " + res;
    }


    public void Generate()
    {
        int rnd = Random.Range(0, 99999);
        PlayerPrefsICloud.SetInt("value", rnd);
        Debug.Log("Saved " + rnd);
        text.text = "Saved " + rnd;
    }

    public void Read()
    {
        int val = PlayerPrefsICloud.GetInt("value", -1);
        Debug.Log("Read " + val);
        text.text = "Read " + val;
    }

    void OnCloudLoadedHandler()
    {
        Debug.Log("Cloud save has loaded!");
    }
}