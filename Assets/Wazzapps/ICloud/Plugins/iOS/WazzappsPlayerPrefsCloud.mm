#import <Foundation/Foundation.h>

#define WAZZAPPS_PREFIX @"WAZZAPPS_CLOUD"
#define WAZZAPPS_GAMEOBJECT "WACloudPrefs"
#define WAZZAPPS_END_METHOD "OnAllValuesLoaded"

extern void UnitySendMessage(const char * gameObject, const char * methodName, const char * message);

@interface PlayerPrefsSyncer : NSObject
- (bool)checkICloudSupported;
- (void)syncToCloud;
- (void)syncToLocal;
@end

PlayerPrefsSyncer * playerPrefsSyncerDelegate12114;

@implementation PlayerPrefsSyncer

/**
 * Check ICloud supported
 */
- (bool) checkICloudSupported
{
    if(NSClassFromString(@"NSUbiquitousKeyValueStore"))
    {
        if([NSUbiquitousKeyValueStore defaultStore])
        {
            NSLog(@"PlayerPrefsAutoSync: iCloud Supported. Initialized.");
            return true;
        }
        else
        {
            NSLog(@"PlayerPrefsAutoSync: iCloud is not enabled.");
            return false;
        }
    }
    else
    {
        NSLog(@"PlayerPrefsAutoSync: iCloud is not supported on this device/OS version.");
        return false;
    }
}

/**
 * Actually synchronize the PlayerPrefs to iCloud
 */
- (void) syncToCloud
{
    NSLog( @"PlayerPrefsAutoSync: Sending to iCloud" );

    // We store the PlayerPrefs in iCloud as the entry "PlayerPrefs"
    NSDictionary* playerPrefsDict = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    NSMutableDictionary* playerPrefsDictFiltered = [NSMutableDictionary dictionaryWithDictionary:@{}];;
    
    [playerPrefsDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSArray *temp = [playerPrefsDict allKeysForObject:obj];
        NSString *keyStr = [temp objectAtIndex:0];
        
        if([keyStr hasPrefix:WAZZAPPS_PREFIX]) {
            [playerPrefsDictFiltered setValue:obj forKey:key];
            NSLog(@"Send %@ = %@", keyStr, [playerPrefsDict valueForKey:key]);
        }
        
    }];
    
    [[NSUbiquitousKeyValueStore defaultStore] setDictionary:playerPrefsDictFiltered forKey: @"PlayerPrefs"];
    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
}

/**
 * iCloud has been updated, so sync to our PlayerPrefs
 */
- (void) syncToLocal
{
    NSLog(@"PlayerPrefsAutoSync: Receiving from iCloud");

    NSUserDefaults* playerPrefs = [NSUserDefaults standardUserDefaults];
    
    // Now all of the keys from iCloud into PlayerPrefs
    NSDictionary *iCloudPrefs = NSUbiquitousKeyValueStore.defaultStore.dictionaryRepresentation;
    NSDictionary* playerPrefsSync = [iCloudPrefs objectForKey:@"PlayerPrefs"];
    if ( playerPrefsSync != nil )
    {
        NSLog(@"playerPrefsSync != nil");
        [playerPrefsSync enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSArray *temp = [playerPrefsSync allKeysForObject:obj];
            NSString *keyStr = [temp objectAtIndex:0];
            
            if([keyStr hasPrefix:WAZZAPPS_PREFIX]) {
                [playerPrefs setObject:obj forKey:key];
                
                NSString * message = [NSString stringWithFormat:@"%@=%@", keyStr, [playerPrefsSync valueForKey:key]];
                NSLog(@"Recieved %@", message);
            }
            
        }];
    }
    [playerPrefs synchronize];
    NSLog(@"Loaded all from cloud");
    
    UnitySendMessage(WAZZAPPS_GAMEOBJECT, WAZZAPPS_END_METHOD, "");
}

@end

extern "C" {
bool WazzappsICloud_CheckICloudSupported() {
    if(playerPrefsSyncerDelegate12114 == NULL) {
        playerPrefsSyncerDelegate12114 = [[PlayerPrefsSyncer alloc] init];
    }
    return [playerPrefsSyncerDelegate12114 checkICloudSupported];
}

void WazzappsICloud_SyncLocalToCloud() {
    if(playerPrefsSyncerDelegate12114 == NULL) {
        playerPrefsSyncerDelegate12114 = [[PlayerPrefsSyncer alloc] init];
    }
    [playerPrefsSyncerDelegate12114 syncToCloud];
}

void WazzappsICloud_SyncCloudToLocal() {
    if(playerPrefsSyncerDelegate12114 == NULL) {
        playerPrefsSyncerDelegate12114 = [[PlayerPrefsSyncer alloc] init];
    }
    [playerPrefsSyncerDelegate12114 syncToLocal];
}
}
