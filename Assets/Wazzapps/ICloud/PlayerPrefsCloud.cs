﻿#if UNITY_IOS
using System;
using UnityEngine;
using System.Runtime.InteropServices;

namespace Wazzapps.ICloud
{
    public static class PlayerPrefsICloud
    {
        [DllImport("__Internal")]
        private static extern bool WazzappsICloud_CheckICloudSupported();

        [DllImport("__Internal")]
        private static extern void WazzappsICloud_SyncLocalToCloud();

        [DllImport("__Internal")]
        private static extern void WazzappsICloud_SyncCloudToLocal();

        private const string Prefix = "WAZZAPPS_CLOUD_";

        public static void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(Prefix + key, value);
            PlayerPrefs.Save();
        }

        public static int GetInt(string key, int defaultValue)
        {
            return PlayerPrefs.GetInt($"{Prefix}{key}", defaultValue);
        }

        public static void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat($"{Prefix}{key}", value);
            PlayerPrefs.Save();
        }

        public static float GetFloat(string key, float defaultValue)
        {
            return PlayerPrefs.GetFloat($"{Prefix}{key}", defaultValue);
        }

        public static void SetString(string key, string value)
        {
            PlayerPrefs.SetString($"{Prefix}{key}", value);
            PlayerPrefs.Save();
        }

        public static string GetString(string key, string defaultValue)
        {
            return PlayerPrefs.GetString($"{Prefix}{key}", defaultValue);
        }

        public static void SaveToCloud()
        {
#if UNITY_EDITOR
            Debug.Log("Dummy save to iCloud");
#else
            WazzappsICloud_SyncLocalToCloud();
#endif
        }

        public static void ReadFromCloud(Action onLoaded)
        {
            if(onLoaded != null) PlayerPrefsICloudGameObject.Init(onLoaded);
#if UNITY_EDITOR
            Debug.Log("Dummy read from iCloud");
            var go = GameObject.Find("WACloudPrefs");
            if (go != null) go.SendMessage("OnAllValuesLoaded", "fake");
#else
            WazzappsICloud_SyncCloudToLocal();
#endif
        }

        public static bool IsCloudEnabled()
        {
#if UNITY_EDITOR
            return true;
#else
            return WazzappsICloud_CheckICloudSupported();
#endif
        }
    }
}
#endif