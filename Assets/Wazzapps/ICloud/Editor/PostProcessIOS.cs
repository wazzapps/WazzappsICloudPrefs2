#if UNITY_IOS && UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace Wazzapps.ICloud
{
    public class EntitlementsPostProcess : ScriptableObject
    {
        [PostProcessBuild]
        public static void OnPostProcess(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget != BuildTarget.iOS)
            {
                return;
            }

            var projPath = PBXProject.GetPBXProjectPath(buildPath);
            var projectPbx = new PBXProject();
            projectPbx.ReadFromFile(projPath);

#if UNITY_2019_3_OR_NEWER
            var targetName = "Unity-iPhone";
            var targetGuid = projectPbx.GetUnityMainTargetGuid();
#else
        var targetName = PBXProject.GetUnityTargetName();
        var targetGuid = projectPBX.TargetGuidByName(targetName);
#endif
            var filename = "ICloud.entitelments";
            FileUtil.CopyFileOrDirectory($"Assets/Wazzapps/ICloud/{filename}", $"{buildPath}/{targetName}/{filename}");
            projectPbx.AddFile($"{targetName}/{filename}", filename);
            projectPbx.AddBuildProperty(targetGuid, "CODE_SIGN_ENTITLEMENTS", $"{targetName}/{filename}");
            projectPbx.WriteToFile(projPath);
        }
    }
}

#endif