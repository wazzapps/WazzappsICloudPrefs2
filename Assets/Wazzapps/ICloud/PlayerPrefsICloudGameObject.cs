﻿using System;
using UnityEngine;

namespace Wazzapps.ICloud
{
    public class PlayerPrefsICloudGameObject : MonoBehaviour
    {
        private Action onLoaded;
        public static void Init(Action onLoaded)
        {
            var instance = new GameObject("WACloudPrefs").AddComponent<PlayerPrefsICloudGameObject>();
            instance.onLoaded = onLoaded;
        }

        public void OnAllValuesLoaded(string message)
        {
            onLoaded?.Invoke();
            Destroy(gameObject);
        }
    }
}