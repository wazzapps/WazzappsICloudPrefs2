# ICloud PlayerPrefs #

Library for save and load data from user's iCloud. Automatically adds ICloud entitlement to XCode project.

### Usage ###

1. Import WazzappsICloudPrefs.unitypackcage
2. Call `Wazzapps.ICloud.PlayerPrefsICloud.ReadFromCloud(onLoaded);` to load all data from iCloud
3. Use `Wazzapps.ICloud.PlayerPrefsICloud.SetString("key", value);` / `SetInt()` / `SetFloat()` to set string/int/float to local PlayerPrefs
4. Call `Wazzapps.ICloud.PlayerPrefsICloud.SaveToCloud();` to sync local data to cloud
5. To check if ICloud is supported, call `bool result = Wazzapps.PlayerPrefsICloud.IsCloudEnabled();`